import React from 'react';


const FormTarea = () => {
	return (
		<div className="formulario">
			<form action="">
				<div className="contenedor-iput" >
					<input
					 	type="text"
					 	className="input-text"
					 	placeholder="Nombre Tarea..."
					 	name="nombre"

					 />
				</div>

				<div className="contenedor-iput" >
					<input
					 	type="submit"
					 	className="btn btn-primario btn-submit btn-block"
					 	value="Agregar Tarea"

					 />
				</div>
			</form>
		</div>
	);
	
}

export default FormTarea;